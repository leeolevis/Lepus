#!/bin/env python
#-*-coding:utf-8-*-

import unittest
import pymysql
from include.mysql_client_class import MySQL

class TestMySQL(unittest.TestCase):
    def setUp(self):
        self.client = MySQL(host = '127.0.0.1',
                       port = 3306,
                       user = 'root',
                       passwd = 'test123',
                       timeout = 28800,
                       charset = 'utf8')
        self.cursor = self.client.db_connect().cursor()

    def test_init(self):
        self.assertIsInstance(self.client, MySQL)

    def test_flush_hosts(self):
        cursor = self.cursor
        self.client.flush_hosts(cursor)

    def test_db_connect(self):
        conn = self.client.db_connect()
        self.assertIsNotNone(conn)
    
    def test_get_mysql_variables(self):
        data_dict = self.client.get_mysql_variables(self.cursor)
        self.assertIsNotNone(data_dict)
    
    def test_get_mysql_version(self):
        version = self.client.get_mysql_version(self.cursor)
        self.assertIsNotNone(version)

if __name__=='__main__':
    unittest.main()
