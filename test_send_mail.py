#!/usr/bin/env python
# -*- coding: utf8 -*-

import sys
path='./include'
sys.path.insert(0,path)
import include.functions as func

send_mail_to_list = func.get_option('send_mail_to_list')
mailto_list=send_mail_to_list.split(';')

result = func.send_mail(mailto_list, "no reply","Beautiful Day")
if result:
    send_mail_status = "success"
else:
    send_mail_status = "fail"
print ("send_mail_status: {}".format(send_mail_status))
