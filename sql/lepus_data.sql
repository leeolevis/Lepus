-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: lepus
-- ------------------------------------------------------
-- Server version	5.7.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `lepus`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `lepus` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `lepus`;

--
-- Dumping data for table `admin_log`
--

LOCK TABLES `admin_log` WRITE;
/*!40000 ALTER TABLE `admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `admin_menu`
--

LOCK TABLES `admin_menu` WRITE;
/*!40000 ALTER TABLE `admin_menu` DISABLE KEYS */;
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (3,'MySQL Monitor',1,0,'lp_mysql','icon-dashboard',0,1,3,'2014-02-25 11:57:29');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (5,'Permission System',1,0,'rabc','icon-legal',0,1,10,'2014-02-26 04:24:33');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (6,'Health Monitor',2,3,'lp_mysql/index',' icon-list',0,1,1,'2014-02-26 04:25:15');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (8,'Replication Monitor',2,3,'lp_mysql/replication',' icon-list',0,1,6,'2014-02-26 04:26:05');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (9,'Slowquery Analysis',2,3,'lp_mysql/slowquery','icon-list',0,1,9,'2014-02-26 04:26:52');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (10,'User',2,5,'user/index','',0,1,1,'2014-02-26 04:43:02');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (11,'Role',2,5,'role/index','',0,1,2,'2014-02-26 04:43:19');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (12,'Menu',2,5,'menu/index','',0,1,3,'2014-02-26 04:43:41');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (13,'Privilege',2,5,'privilege/index','',0,1,4,'2014-02-26 04:45:01');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (15,'Authorization',2,5,'auth/index','',0,1,2,'2014-03-03 14:23:28');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (16,'Servers Configure',1,0,'server','icon-dashboard',0,1,2,'2014-03-05 10:31:17');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (18,'MySQL',2,16,'servers_mysql/index','icon-list',0,1,3,'2014-03-05 10:33:40');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (19,'AWR Report',2,3,'lp_mysql/awrreport','icon-list',0,1,12,'2014-03-06 05:47:17');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (20,'Alarm Panel',1,0,'alarm','icon-dashboard',0,1,9,'2014-03-11 13:41:13');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (21,'Alarm List',2,20,'alarm/index','',0,1,0,'2014-03-11 13:46:28');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (22,'OS Monitor',1,0,'lp_os','icon-dashboard',0,1,8,'2014-03-24 07:33:42');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (26,'Disk',2,22,'lp_os/disk','icon-list',0,1,4,'2014-03-24 09:46:29');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (28,'BigTable Analysis',2,3,'lp_mysql/bigtable','icon-list',0,1,7,'2014-04-02 05:38:15');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (29,'Key Cache Monitor',2,3,'lp_mysql/key_cache','icon-list',0,1,3,'2014-04-09 07:52:12');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (30,'InnoDB Monitor',2,3,'lp_mysql/innodb','icon-list',0,1,4,'2014-04-09 07:54:47');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (31,'Resource Monitor',2,3,'lp_mysql/resource','icon-list',0,1,2,'2014-04-10 05:23:06');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (40,'Oracle',2,16,'servers_oracle/index','icon-list',0,1,4,'2014-05-27 05:21:49');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (43,'Health Monitor',2,22,'lp_os/index','icon-list',0,1,0,'2014-07-08 01:19:11');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (44,'Disk IO',2,22,'lp_os/disk_io','icon-list',0,1,5,'2014-07-15 07:35:56');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (45,'OS',2,16,'servers_os/index','icon-list',0,1,8,'2014-07-16 02:32:13');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (46,'Settings',2,16,'settings/index','icon-list',0,1,0,'2014-08-12 07:30:54');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (56,'Oracle Monitor',1,0,'lp_oracle','icon-dashboard',0,1,4,'2014-10-24 07:34:50');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (57,'Health Montior',2,56,'lp_oracle/index','icon-list',0,1,1,'2014-10-24 07:35:47');
INSERT INTO `admin_menu` (`menu_id`, `menu_title`, `menu_level`, `parent_id`, `menu_url`, `menu_icon`, `system`, `status`, `display_order`, `create_time`) VALUES (58,'Tablespace Monitor',2,56,'lp_oracle/tablespace','icon-list',0,1,2,'2014-10-24 07:37:19');
/*!40000 ALTER TABLE `admin_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `admin_privilege`
--

LOCK TABLES `admin_privilege` WRITE;
/*!40000 ALTER TABLE `admin_privilege` DISABLE KEYS */;
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (1,'MySQL Health Monitor',6,'lp_mysql/index',1);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (3,'MySQL Replication Monitor',8,'lp_mysql/replication',2);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (4,'MySQLSlowQuery',9,'lp_mysql/slowquery',4);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (6,'Admin User View',10,'user/index',52);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (7,'Admin User Add ',10,'user/add',52);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (8,'Admin User Edit',10,'user/edit',53);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (9,'Admin User Delete',10,'user/forever_delete',54);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (10,'Admin Role View',11,'role/index',61);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (11,'Admin Role Add',11,'role/add',62);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (12,'Admin Role Edit',11,'role/edit',63);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (13,'Admin Role Delete',11,'role/forever_delete',64);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (14,'Admin Menu View',12,'menu/index',71);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (15,'Admin Menu Add',12,'menu/add',72);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (16,'Admin Menu Edit',12,'menu/edit',73);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (17,'Admin Menu Delete',12,'menu/forever_delete',74);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (18,'Admin Privilege View',13,'privilege/index',81);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (19,'Admin Privilege Add',13,'privilege/add',82);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (20,'Admin Privilege Edit',13,'privilege/edit',83);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (21,'Admin Privilege Delete',13,'privilege/forever_delete',84);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (22,'Admin Auth View',15,'auth/index',91);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (23,'Admin Role Privilege Update',15,'auth/update_role_privilege',92);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (24,'Login System',0,'index/index',0);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (25,'Admin User Role Update',13,'auth/update_user_role',93);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (31,'MySQL Servers View',18,'servers_mysql/index',36);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (32,'MySQL Servers Add',18,'servers_mysql/add',37);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (33,'MySQL Servers Edit',18,'servers_mysql/edit',38);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (34,'MySQL Servers Trash',18,'servers_mysql/trash',39);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (35,'MySQL Servers Delete',18,'servers_mysql/delete',40);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (36,'MySQLSlowQuery Detail',9,'lp_mysql/slowquery_detail',4);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (37,'MySQL AWR Report',19,'lp_mysql/awrreport',5);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (38,'MySQL Health Chart',6,'lp_mysql/chart',1);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (39,'MySQL Replication Chart',8,'lp_mysql/replication_chart',2);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (40,'Alarm View',21,'alarm/index',8);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (41,'OS Health View',43,'lp_os/index',100);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (44,'OS Disk View',26,'lp_os/disk',100);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (46,'OS Disk Chart View',26,'lp_os/disk_chart',100);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (48,'OS Health Chart View',43,'lp_os/chart',100);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (49,'MySQL BigTable Analysis',28,'lp_mysql/bigtable',8);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (50,'MySQL BigTable Analysis Chart',28,'lp_mysql/bigtable_chart',8);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (51,'MySQL Key Cache Monitor',29,'lp_mysql/key_cache',2);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (52,'MySQL InnoDB Monitor',30,'lp_mysql/innodb',2);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (53,'MySQL Resource Monitor',31,'lp_mysql/resource',2);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (67,'Oracle Servers View',40,'servers_oracle/index',45);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (68,'Oracle Servers Add',40,'servers_oracle/add',46);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (69,'Oracle Servers Edit',40,'servers_oracle/edit',47);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (77,'OS Disk View',44,'lp_os/disk_io',100);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (78,'OS Disk Chart View',44,'lp_os/disk_io_chart',100);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (79,'OS Servers View',45,'servers_os/index',50);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (80,'OS  Servers Add',45,'servers_os/add',50);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (81,'OS Servers Edit',45,'servers_os/edit',50);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (82,'OS Servers Delete',45,'servers_os/delete',50);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (83,'OS Servers Trash',45,'servers_os/trash',50);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (84,'OS Servers Batch Add',45,'servers_os/batch_add',50);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (86,'MySQL Servers Batch Add',18,'servers_mysql/batch_add',40);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (87,'Settings View',46,'settings/index',30);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (110,'Oracle Health Monitor',57,'lp_oracle/index',25);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (111,'Oracle Health Chart',57,'lp_oracle/chart',26);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (112,'Oracle Tablespace Monitor',58,'lp_oracle/tablespace',27);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (113,'Settings Save',46,'settings/save',31);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (114,'Oracle Servers Trash',40,'servers_oracle/trash',48);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (115,'Oracle Servers Delete',40,'servers_oracle/delete',48);
INSERT INTO `admin_privilege` (`privilege_id`, `privilege_title`, `menu_id`, `action`, `display_order`) VALUES (116,'Oracle Servers Batch Add',40,'servers_oracle/batch_add',48);
/*!40000 ALTER TABLE `admin_privilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `admin_role`
--

LOCK TABLES `admin_role` WRITE;
/*!40000 ALTER TABLE `admin_role` DISABLE KEYS */;
INSERT INTO `admin_role` (`role_id`, `role_name`) VALUES (1,'Administrator');
INSERT INTO `admin_role` (`role_id`, `role_name`) VALUES (3,'IT-DBA');
INSERT INTO `admin_role` (`role_id`, `role_name`) VALUES (5,'IT-Developer');
INSERT INTO `admin_role` (`role_id`, `role_name`) VALUES (7,'guest_group');
/*!40000 ALTER TABLE `admin_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `admin_role_privilege`
--

LOCK TABLES `admin_role_privilege` WRITE;
/*!40000 ALTER TABLE `admin_role_privilege` DISABLE KEYS */;
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,1);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,3);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,4);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,6);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,7);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,8);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,9);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,10);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,11);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,12);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,13);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,14);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,15);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,16);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,17);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,18);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,19);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,20);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,21);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,22);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,23);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,24);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,25);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,31);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,32);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,33);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,34);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,35);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,36);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,37);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,38);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,39);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,40);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,41);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,44);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,46);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,48);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,49);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,50);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,51);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,52);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,53);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,54);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,55);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,56);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,57);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,58);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,59);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,60);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,61);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,67);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,68);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,69);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,76);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,77);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,78);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,79);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,80);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,81);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,82);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,83);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,84);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,85);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,86);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,87);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,92);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,93);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,94);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,95);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,96);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,97);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,98);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,99);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,100);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,101);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,104);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,105);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,110);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,111);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,112);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,113);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,114);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,115);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,116);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,117);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,118);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,119);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,120);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,121);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,122);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,123);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (1,124);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (2,4);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,1);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,2);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,3);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,4);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,6);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,7);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,8);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,9);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,10);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,11);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,12);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,13);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,14);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,15);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,16);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,17);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,18);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,19);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,20);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,21);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,22);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,23);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,24);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,25);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,26);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,27);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,28);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,29);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,30);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,31);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,32);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,33);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,34);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,35);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,36);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,37);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,38);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,39);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,40);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,41);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,42);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,43);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,44);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,46);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,47);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,48);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,49);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,50);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,51);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,52);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,53);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,54);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,55);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,56);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,57);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,58);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,59);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,60);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,61);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,67);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,68);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,69);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,70);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,71);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,72);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,74);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,75);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,76);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,77);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,78);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,79);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,80);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,81);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,82);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,83);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,84);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,85);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,86);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,87);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,88);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,89);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (3,90);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,1);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,3);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,4);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,24);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,36);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,38);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,39);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,42);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,43);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,44);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,46);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,47);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,48);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,59);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,60);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,61);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,74);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,75);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,76);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,77);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,78);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,88);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,89);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (5,90);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,1);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,3);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,4);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,6);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,10);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,14);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,18);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,22);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,24);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,36);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,37);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,38);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,39);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,40);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,41);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,44);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,46);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,48);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,49);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,50);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,51);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,52);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,53);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,59);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,60);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,61);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,76);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,77);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,78);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,87);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,92);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,93);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,100);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,101);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,104);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,105);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,110);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,111);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,112);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,117);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,123);
INSERT INTO `admin_role_privilege` (`role_id`, `privilege_id`) VALUES (7,124);
/*!40000 ALTER TABLE `admin_role_privilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `admin_user`
--

LOCK TABLES `admin_user` WRITE;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
INSERT INTO `admin_user` (`user_id`, `username`, `password`, `realname`, `email`, `mobile`, `login_count`, `last_login_ip`, `last_login_time`, `status`, `create_time`) VALUES (1,'admin','6f493fbddf9107797f5044bb229ac6ee','Administrator','admin@mail.com','',48,'42.196.163.128','2016-05-01 09:53:36',1,'2013-12-25 07:58:34');
INSERT INTO `admin_user` (`user_id`, `username`, `password`, `realname`, `email`, `mobile`, `login_count`, `last_login_ip`, `last_login_time`, `status`, `create_time`) VALUES (8,'guest','e10adc3949ba59abbe56e057f20f883e','Guest','','',5664,'42.196.163.128','2016-05-01 09:28:46',1,'2014-03-12 09:06:36');
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `admin_user_role`
--

LOCK TABLES `admin_user_role` WRITE;
/*!40000 ALTER TABLE `admin_user_role` DISABLE KEYS */;
INSERT INTO `admin_user_role` (`user_id`, `role_id`) VALUES (1,1);
INSERT INTO `admin_user_role` (`user_id`, `role_id`) VALUES (2,1);
INSERT INTO `admin_user_role` (`user_id`, `role_id`) VALUES (2,2);
INSERT INTO `admin_user_role` (`user_id`, `role_id`) VALUES (2,3);
INSERT INTO `admin_user_role` (`user_id`, `role_id`) VALUES (2,5);
INSERT INTO `admin_user_role` (`user_id`, `role_id`) VALUES (8,7);
INSERT INTO `admin_user_role` (`user_id`, `role_id`) VALUES (9,3);
/*!40000 ALTER TABLE `admin_user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `alarm`
--

LOCK TABLES `alarm` WRITE;
/*!40000 ALTER TABLE `alarm` DISABLE KEYS */;
/*!40000 ALTER TABLE `alarm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `alarm_history`
--

LOCK TABLES `alarm_history` WRITE;
/*!40000 ALTER TABLE `alarm_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `alarm_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `alarm_temp`
--

LOCK TABLES `alarm_temp` WRITE;
/*!40000 ALTER TABLE `alarm_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `alarm_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `db_servers_mongodb`
--

LOCK TABLES `db_servers_mongodb` WRITE;
/*!40000 ALTER TABLE `db_servers_mongodb` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_servers_mongodb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `db_servers_mysql`
--

LOCK TABLES `db_servers_mysql` WRITE;
/*!40000 ALTER TABLE `db_servers_mysql` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_servers_mysql` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `db_servers_oracle`
--

LOCK TABLES `db_servers_oracle` WRITE;
/*!40000 ALTER TABLE `db_servers_oracle` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_servers_oracle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `db_servers_os`
--

LOCK TABLES `db_servers_os` WRITE;
/*!40000 ALTER TABLE `db_servers_os` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_servers_os` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `db_servers_redis`
--

LOCK TABLES `db_servers_redis` WRITE;
/*!40000 ALTER TABLE `db_servers_redis` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_servers_redis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `db_servers_sqlserver`
--

LOCK TABLES `db_servers_sqlserver` WRITE;
/*!40000 ALTER TABLE `db_servers_sqlserver` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_servers_sqlserver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `db_status`
--

LOCK TABLES `db_status` WRITE;
/*!40000 ALTER TABLE `db_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `db_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `lepus_status`
--

LOCK TABLES `lepus_status` WRITE;
/*!40000 ALTER TABLE `lepus_status` DISABLE KEYS */;
INSERT INTO `lepus_status` (`lepus_variables`, `lepus_value`) VALUES ('lepus_running','1');
INSERT INTO `lepus_status` (`lepus_variables`, `lepus_value`) VALUES ('lepus_version','3.8 Beta');
INSERT INTO `lepus_status` (`lepus_variables`, `lepus_value`) VALUES ('lepus_checktime','2016-05-01 09:56:10');
/*!40000 ALTER TABLE `lepus_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mongodb_status`
--

LOCK TABLES `mongodb_status` WRITE;
/*!40000 ALTER TABLE `mongodb_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `mongodb_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mongodb_status_history`
--

LOCK TABLES `mongodb_status_history` WRITE;
/*!40000 ALTER TABLE `mongodb_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `mongodb_status_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_bigtable`
--

LOCK TABLES `mysql_bigtable` WRITE;
/*!40000 ALTER TABLE `mysql_bigtable` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_bigtable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_bigtable_history`
--

LOCK TABLES `mysql_bigtable_history` WRITE;
/*!40000 ALTER TABLE `mysql_bigtable_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_bigtable_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_connected`
--

LOCK TABLES `mysql_connected` WRITE;
/*!40000 ALTER TABLE `mysql_connected` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_connected` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_processlist`
--

LOCK TABLES `mysql_processlist` WRITE;
/*!40000 ALTER TABLE `mysql_processlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_processlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_replication`
--

LOCK TABLES `mysql_replication` WRITE;
/*!40000 ALTER TABLE `mysql_replication` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_replication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_replication_history`
--

LOCK TABLES `mysql_replication_history` WRITE;
/*!40000 ALTER TABLE `mysql_replication_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_replication_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_slow_query_review`
--

LOCK TABLES `mysql_slow_query_review` WRITE;
/*!40000 ALTER TABLE `mysql_slow_query_review` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_slow_query_review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_slow_query_review_history`
--

LOCK TABLES `mysql_slow_query_review_history` WRITE;
/*!40000 ALTER TABLE `mysql_slow_query_review_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_slow_query_review_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_slow_query_sendmail_log`
--

LOCK TABLES `mysql_slow_query_sendmail_log` WRITE;
/*!40000 ALTER TABLE `mysql_slow_query_sendmail_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_slow_query_sendmail_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_status`
--

LOCK TABLES `mysql_status` WRITE;
/*!40000 ALTER TABLE `mysql_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mysql_status_history`
--

LOCK TABLES `mysql_status_history` WRITE;
/*!40000 ALTER TABLE `mysql_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `mysql_status_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `options`
--

LOCK TABLES `options` WRITE;
/*!40000 ALTER TABLE `options` DISABLE KEYS */;
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('monitor','1','是否开启全局监控,此项如果关闭则所有项目都不会被监控，下面监控选项都失效');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('monitor_mysql','1','是否开启MySQL状态监控');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('send_alarm_mail','1','是否发送报警邮件');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('send_mail_to_list','','报警邮件通知人员');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('monitor_os','0','是否开启OS监控');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('alarm','1','是否开启告警');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('send_mail_max_count','3','发送邮件最大次数');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('report_mail_to_list','','报告邮件推送接收人员');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('frequency_monitor','60','监控频率');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('send_mail_sleep_time','720','发送邮件休眠时间(分钟)');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('mailtype','html','邮件发送配置:邮件类型');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('mailprotocol','smtp','邮件发送配置:邮件协议');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('smtp_host','smtp.126.com','邮件发送配置:邮件主机');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('smtp_port','25','邮件发送配置:邮件端口');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('smtp_user','noreplymail','邮件发送配置:用户');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('smtp_pass','','邮件发送配置:密码');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('smtp_timeout','10','邮件发送配置:超时时间');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('mailfrom','noreplymail@126.com','邮件发送配置:发件人');
INSERT INTO `options` (`name`, `value`, `description`) VALUES ('monitor_oracle','0','是否监控Oracle');
/*!40000 ALTER TABLE `options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `oracle_status`
--

LOCK TABLES `oracle_status` WRITE;
/*!40000 ALTER TABLE `oracle_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `oracle_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `oracle_status_history`
--

LOCK TABLES `oracle_status_history` WRITE;
/*!40000 ALTER TABLE `oracle_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `oracle_status_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `oracle_tablespace`
--

LOCK TABLES `oracle_tablespace` WRITE;
/*!40000 ALTER TABLE `oracle_tablespace` DISABLE KEYS */;
/*!40000 ALTER TABLE `oracle_tablespace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `oracle_tablespace_history`
--

LOCK TABLES `oracle_tablespace_history` WRITE;
/*!40000 ALTER TABLE `oracle_tablespace_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `oracle_tablespace_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `os_disk`
--

LOCK TABLES `os_disk` WRITE;
/*!40000 ALTER TABLE `os_disk` DISABLE KEYS */;
/*!40000 ALTER TABLE `os_disk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `os_disk_history`
--

LOCK TABLES `os_disk_history` WRITE;
/*!40000 ALTER TABLE `os_disk_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `os_disk_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `os_diskio`
--

LOCK TABLES `os_diskio` WRITE;
/*!40000 ALTER TABLE `os_diskio` DISABLE KEYS */;
/*!40000 ALTER TABLE `os_diskio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `os_diskio_history`
--

LOCK TABLES `os_diskio_history` WRITE;
/*!40000 ALTER TABLE `os_diskio_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `os_diskio_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `os_net`
--

LOCK TABLES `os_net` WRITE;
/*!40000 ALTER TABLE `os_net` DISABLE KEYS */;
/*!40000 ALTER TABLE `os_net` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `os_net_history`
--

LOCK TABLES `os_net_history` WRITE;
/*!40000 ALTER TABLE `os_net_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `os_net_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `os_status`
--

LOCK TABLES `os_status` WRITE;
/*!40000 ALTER TABLE `os_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `os_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `os_status_history`
--

LOCK TABLES `os_status_history` WRITE;
/*!40000 ALTER TABLE `os_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `os_status_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `redis_replication`
--

LOCK TABLES `redis_replication` WRITE;
/*!40000 ALTER TABLE `redis_replication` DISABLE KEYS */;
/*!40000 ALTER TABLE `redis_replication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `redis_replication_history`
--

LOCK TABLES `redis_replication_history` WRITE;
/*!40000 ALTER TABLE `redis_replication_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `redis_replication_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `redis_status`
--

LOCK TABLES `redis_status` WRITE;
/*!40000 ALTER TABLE `redis_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `redis_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `redis_status_history`
--

LOCK TABLES `redis_status_history` WRITE;
/*!40000 ALTER TABLE `redis_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `redis_status_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sqlserver_status`
--

LOCK TABLES `sqlserver_status` WRITE;
/*!40000 ALTER TABLE `sqlserver_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `sqlserver_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sqlserver_status_history`
--

LOCK TABLES `sqlserver_status_history` WRITE;
/*!40000 ALTER TABLE `sqlserver_status_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `sqlserver_status_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-16 22:30:48
