#!/usr/bin/env python
# coding:utf-8

import unittest
from check_mysql_bigtable import check_mysql_bigtable


class TestCheckMysqlBigtable(unittest.TestCase):
    def test_check_mysql_bigtable(self):
        check_mysql_bigtable(host='127.0.0.1',
                             port='3306',
                             username='root',
                             password='test123',
                             server_id=1,
                             tags='test',
                             bigtable_size=1000)


if __name__ == '__main__':
    unittest.main()
